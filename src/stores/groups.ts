// stores/counter.js
import { defineStore } from 'pinia'
import { Element } from '../utils/badge'

export const useGroupsStore = defineStore('groupsStore', {
    state: () => ({
        displaySinglePurpose: "none",
        donateDialogVisible: false,
        donateDialogID: 0,
        groups: [
            {
                id: 0,
                name: "Corporación Amazona",
                pic: "/groups/amazonas.jpg",
                banner: "/groups/amazonas-banner.jpg",
                username: "corpama92",
                description: "The purpose of our organization is to raise awareness of cultural values and the particular needs of ethnic minorities.",
                joinDate: new Date(2021, 2, 2),
                category: "Reforestation",
                likes: 134,
                funded: 800,
                fundingGoal: 2000,
                posts: [
                    {
                        timestamp: +new Date,
                        content: "We build a new compost toilet at a rural village. Follow us for more updates.",
                    },
                    {
                        timestamp: +new Date,
                        content: "Our mission is to help improve lives of ethnic minorities in Brazil",
                    }
                ],
                comments: [{
                    id: 0,
                    userID: 0,
                    text: "Wow so beautful!"
                },
                {
                    id: 1,
                    userID: 1,
                    text: "I want to give them all my money."
                }],
                element: Element.Earth,
                badge: 0,
            },
            {
                id: 1,
                name: "Seal Rescue",
                pic: "/groups/seal.jpg",
                banner: "/groups/seal-banner.jpg",
                username: "sealrescue2022",
                description: "Our mission dedicates to the rescue, rehabilitation and release of sick or injured seals.",
                joinDate: new Date(2022, 1, 6),
                category: "Animals",
                likes: 26,
                funded: 250,
                fundingGoal: 34000,
                posts: [
                    {
                        timestamp: +new Date,
                        content: "Help us save as many seals as possible!",
                        comments: [],
                    },
                ],
                comments: [{
                    id: 0,
                    userID: 1,
                    text: "Show more seals please."
                }],
                element: Element.Water,
                badge: 2,
            },
            {
                id: 2,
                name: "Food Sovereignity Nigeria",
                pic: "/groups/garden.jpg",
                banner: "/groups/garden-banner.jpg",
                username: "nigeriangardens",
                description: "We dedicate our time to grow the regenerative farming sector in Nigeria",
                joinDate: new Date(2018, 12, 9),
                category: "Food",
                likes: 178,
                funded: 22000,
                fundingGoal: 50000,
                posts: [],
                comments: [],
                element: Element.Earth,
                badge: 1,
            },
            {
                id: 3,
                name: "Freeshop in Berlin",
                pic: "/groups/freeshop.jpg",
                banner: "/groups/freeshop-banner.jpg",
                username: "berlinerfreeshop72",
                description: "We are facing closure due to new government regulations and are asking for support",
                joinDate: new Date(2022, 1, 29),
                category: "Retail",
                likes: 750,
                funded: 500,
                fundingGoal: 3000,
                posts: [],
                comments: [],
                element: Element.Space,
                badge: 3,
            },
        ]
    })
})


