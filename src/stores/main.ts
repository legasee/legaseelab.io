import { defineStore } from 'pinia'

export const useMainStore = defineStore('mainStore', {
    state: () => ({
        accountType: "user",
        currentUserID: 0,
        showSingleBadge: "none",
        selectedBadge: 0,
    })
})
