// stores/counter.js
import { defineStore } from 'pinia'
import { Rarity } from '../utils/badge'

const elaisBadges = [
    {
        badgeId: 7,
        rarity: Rarity.Unique,
        xp: 20,
        xpGoal: 100,
        level: 2,
        id: 7,
    },
    {
        badgeId: 6,
        rarity: Rarity.Elemental,
        xp: 72,
        xpGoal: 100,
        level: 1,
        id: 6,
    },
    {
        badgeId: 5,
        rarity: Rarity.Elemental,
        xp: 23,
        xpGoal: 100,
        level: 3,
        id: 5,
    },
    {
        badgeId: 4,
        rarity: Rarity.Elemental,
        xp: 12,
        xpGoal: 100,
        level: 5,
        id: 4,
    },
    {
        badgeId: 3,
        rarity: Rarity.Legendary,
        xp: 14,
        xpGoal: 100,
        level: 65,
        id: 3,
    },
    {
        badgeId: 2,
        rarity: Rarity.Epic,
        xp: 63,
        xpGoal: 100,
        level: 24,
        id: 2,
    },
    {
        badgeId: 1,
        rarity: Rarity.Rare,
        xp: 99,
        xpGoal: 100,
        level: 643,
        id: 1,
    },
    {
        badgeId: 0,
        rarity: Rarity.Common,
        xp: 0,
        xpGoal: 100,
        level: 64,
        id: 0,
    }]

export const useUserStore = defineStore('userStore', {
    state: () => ({
        users: [{
            id: 0,
            name: "Elias Hvidsten",
            xp: 78,
            xpGoal: 100,
            level: 100,
            pic: "elias.jpg",
            banner: "elias-banner.jpg",
            username: "elias72",
            bio: "Hello, my name is Elias. Thanks to legasee I can keep up to date with any progress of Widlife NGO in South america. I love it here!",
            joinDate: new Date(2022, 8, 2),
            posts: [
            {
                timestamp: +new Date,
                content: "Just arrived in Colombia! I can't wait to explore",
            },
            {
                timestamp: +new Date,
                content: "I finished my coffe, getting ready to fly now",
            }
            ],
            liked: [{
                groupID: 1,
                donated: 10,
            },
            {
                groupID: 3,
                donated: 300,
            }],
            badges: elaisBadges,
            defaultDonate: 1,
            currentBalance: 494,
            pins: [
                {
                    type: 'org',
                    id: 0,
                },
                {
                    type: 'org',
                    id: 3,
                }
            ]
        },
        {
            id: 1,
            name: "Rak Vanich",
            pic: "rak.jpg",
            banner: "rak-banner.jpg",
            username: "elias72",
            bio: "I'm Rak, I love travel!",
            joinDate: new Date(2019, 2, 5),
            posts: [
            {
                timestamp: +new Date,
                content: "Currently in Malaysia, the landscapes are unreal.",
            }
            ],
            liked: [{
                groupID: 0,
                donated: 100,
            },
            ],
            defaultDonate: 100,
            currentBalance: 10900,
        }
    ]
        
    })
})


