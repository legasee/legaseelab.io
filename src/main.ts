import { createApp } from "vue";
import { createPinia } from 'pinia'
import App from "./App.vue";
import router from './router'

import PrimeVue from 'primevue/config';

import "primevue/resources/themes/lara-dark-purple/theme.css"
import "primevue/resources/primevue.min.css"
import "primeicons/primeicons.css"
import "primeflex/primeflex.css"

import "./styles/index.css"

const pinia = createPinia()
const app = createApp(App);

app.use(PrimeVue);

app.use(pinia)
app.use(router);

app.mount("#app");
