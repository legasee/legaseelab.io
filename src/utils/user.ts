type Like = {
    groupID: number,
    donated: number,
}

type User = {
    liked: Like[]
}

export const getTotalDonated = (user: User) => {
    let total = 0
    user.liked.forEach(like => {
        total += like.donated
    });
    return total
}

export const getHighestDonated = (user: User) => {
    let max = 0
    user.liked.forEach(like => {
        if (like.donated > max) {
            max = like.donated
        }
    });
    return max
}
