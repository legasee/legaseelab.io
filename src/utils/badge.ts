export enum Rarity {
    Common,
    Rare,
    Epic,
    Legendary,
    Elemental,
    Unique,
}

export enum Element {
    Earth,
    Water,
    Fire,
    Air,
    Space,
    Consciousness
}

export type Badge = {
    rarity: Rarity,
    element: Element,
    name: string,
    level: number,
    xp: number
}

type User = {
    badges: Badge[]
}

type Group = {
    element: Element,
    badge: number,
}

export const badges = [
    {
        id: 0,
        groupId: 0,
        rarity: Rarity.Common,
        name: 'Amazonas Badge',
        file_path: "amazon.png"
    },
    {
        id: 1,
        groupId: 2,
        rarity: Rarity.Common,
        name: 'Garden Badge',
        file_path: "garden.png"
    },
    {
        id: 2,
        groupId: 1,
        rarity: Rarity.Common,
        name: 'Seal Badge',
        file_path: "seal.png"
    },
    {
        id: 3,
        groupId: 3,
        rarity: Rarity.Common,
        name: 'Freeshop Badge',
        file_path: "freeshop.png"
    },
    {
        id: 4,
        rarity: Rarity.Elemental,
        name: 'Earth Badge',
        element: Element.Earth,
        file_path: "1.png"
    },
    {
        id: 5,
        rarity: Rarity.Elemental,
        name: 'Fire Badge',
        element: Element.Fire,
        file_path: "2.png"
    },
    {
        id: 6,
        name: 'Water Badge',
        rarity: Rarity.Elemental,
        element: Element.Water,
        file_path: "3.png"
    },
    {
        id: 7,
        rarity: Rarity.Unique,
        name: 'Investor Badge',
        file_path: "investor.png",
    }
]

export const getElementBadges = (groups: Group[], element: Element) => {
    let rightElementGroups = groups.filter((group) => {
        return group.element == element
    })
    return rightElementGroups
}

export const getGroupBadge = (group: Group) => {
    return badges.find(badge => badge.id == group.badge)

}

export const getBestBadge = (user: User) => {
    let best = {
        rarity: Rarity.Common,
        name: "",
        level: 0,
    }
    user.badges.forEach(badge => {
        if (badge.rarity > best.rarity && badge.rarity <= Rarity.Legendary) {
            best = badge
        }
        else if( badge.rarity == best.rarity && badge.level > best.level) {
            best = badge
        }
    })
    return best
}


export const getBadges = (user: User, typ: number) => {
    let badges = user.badges.filter((el) => {

        return el.rarity == typ
    })
    badges = badges.sort((a, b) => {
        if (b.level == a.level) {
            return b.xp - a.xp
        } 
        else {
            return b.level - a.level
        }
    })
    return badges
}