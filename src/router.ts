import { createRouter, createWebHistory } from 'vue-router'
import Home from './components/pages/Home.vue'
import Badges from './components/pages/Badges.vue'
import Logout from './components/pages/Logout.vue'
import Purpose from './components/pages/Purpose.vue'
import User from './components/pages/User.vue'
import Organization from './components/pages/Organization.vue'

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home,
    },
    {
        path: '/user/:id',
        name: 'User',
        component: User,
    },
    {
        path: '/badges',
        name: 'Badges',
        component: Badges,
    },
    {
        path: '/logout',
        name: 'Logout',
        component: Logout,
    },
    {
        path: '/organization/:id',
        name: 'Organization',
        component: Organization,
    },
    {
        path: '/purpose',
        name: 'Purpose',
        component: Purpose,
    }
]


const router = createRouter({
    history: createWebHistory('/#'),
    routes,
})

export default router